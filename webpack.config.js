const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");

module.exports = (env, { mode }) => ({
  mode,
  target: "web",
  entry: {
    app: path.resolve(__dirname, "src", "index.tsx"),
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].js",
  },
  resolve: {
    mainFiles: ["index"],
    modules: ["node_modules", "lib", "shared-components", "shared-types"],
    alias: {
      store: path.resolve(__dirname, "src", "store"),
      "react-dom": "@hot-loader/react-dom",
      commonStatics: path.resolve(__dirname, "src", "statics"),
    },
    extensions: [".ts", ".tsx", ".js", ".jsx"],
  },
  module: {
    rules: [
      {
        test: /\.(j|t)s(x)?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            cacheDirectory: true,
            babelrc: false,
            presets: [
              [
                "@babel/preset-env",
                { targets: { browsers: "last 2 versions" } },
              ],
              "@babel/preset-typescript",
              "@babel/preset-react",
            ],
            plugins: [
              ["@babel/plugin-proposal-decorators", { legacy: true }],
              ["@babel/plugin-proposal-class-properties", { loose: true }],
              "@babel/plugin-transform-runtime",
              "react-hot-loader/babel",
            ],
          },
        },
      },
      {
        test: /\.preload\.(png|jpg|gif|ttf|woff|woff2|eot|svg|otf)$/,
        use: ["url-loader"],
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: (filePath) =>
          /\.(webp|ttf|woff|woff2|eot|svg|otf|png|jpg|gif)$/.test(filePath) &&
          !/\.preload\.(webp|ttf|woff|woff2|eot|svg|otf|png|jpg|gif)$/.test(
            filePath
          ) &&
          !/\.raw\.svg$/.test(filePath),
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[hash].[ext]",
              outputPath: "statics/",
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.ProvidePlugin({
      React: "react",
    }),
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, "dist", "index.html"),
      template: "./index.html",
    }),
    new ForkTsCheckerWebpackPlugin({
      eslint: {
        files: "./src/**/*.{ts,tsx}",
      },
    }),
  ],
  ...(mode === "development"
    ? {
        devServer: {
          port: 9000,
          hot: true,
          inline: true,
        },
      }
    : {}),
});
