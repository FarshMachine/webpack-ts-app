import { Switch, Route } from "react-router-dom";
import { hot } from "react-hot-loader/root";
import { ThemeLayout } from "@abdt/ornament";
import Main from "./pages/Main";

const App = () => (
  <ThemeLayout>
    <Switch>
      <Route exact path="/" component={Main} />
    </Switch>
  </ThemeLayout>
);

export default hot(App);
