import React from "react";
import Main from "./style";

const MainPageComponent = () => (
  <Main>
    <div>Hello dude</div>
    <ul>
      <li>Первый пункс</li>
    </ul>
  </Main>
);

export default React.memo(MainPageComponent);
