module.exports = {
  extends: ["abdt", "abdt/react", "abdt/typescript"],
  globals: {
    React: true,
  },
  overrides: [
    {
      files: ["webpack.config.js"],
      rules: {
        "@typescript-eslint/no-var-requires": "off",
      },
    },
    {
      files: ["*.ts", "*.tsx"],
      rules: {
        "react/jsx-no-undef": [2, { allowGlobals: true }],
        "react/react-in-jsx-scope": "off",
      },
    },
  ],
};
